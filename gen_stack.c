#include "gen_stack.h"

STK * create_s (void) {
    STK * f = (STK *) malloc(sizeof(STK)); // Aloca espaço para a pilha
    f->ini = NULL; // Ponteiro de início são anulados
    return f; // Retorna a pilha
}

void push( STK * f) {
    List * n = (List *)malloc(sizeof(List)); // Aloca espaço para o nó
    n->prox = f->ini; // Ponteiro para o próximo é o antigo primeiro da pilha
    memset(n->node, 0, 100000000);
    f->ini = n; // O começo apontará para n
}

void pop(STK * f) {
    List * t; // Inicia ponteiro auxiliar
    void * node; // Inicia valor para retornar
    if(empty_s(f)) {
        printf("Fila vazia!\n");
        return; // Retorna para onde a função foi chamada
    }
    t = f->ini; // Ponteiro t aponta para o primeiro nó
    node = t->node; // Valor recebe o que está no campo node do nó t
    f->ini = t->prox; // Primeiro da fila passa a ser o segundo nó da fila
    free(t); // Libera o nó de t, que ocupava a primeira posição na fila
    return; // Retorna valor que estava no nó
}

int empty_s(STK * f) {
    return (f->ini == NULL); // Retorna se o começo da fila existe ou não
}

void free_s(STK * f) {
    if(f->ini == NULL){
        free(f); // Libera a estrutura fila, com os campos ini e fim
        return;
    }
    List * q = f->ini; // Cria um nó para iteração, apontando para o começo da fila
    List * t; // Cria um ponteiro auxiliar
    while(q) {
        t = q->prox; // Ponteiro auxiliar recebe o próximo de q
        free(q); // Libera q
        q = t; // Atualiza q com o valor de seu próximo, que estava salvo em t
    }
    free(f); // Libera a fila
}