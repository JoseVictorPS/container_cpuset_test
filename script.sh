START=$(docker inspect --format='{{.State.StartedAt}}' stackum)
STOP=$(docker inspect --format='{{.State.FinishedAt}}' stackum)

START_TIMESTAMP=$(date --date=$START +%s)
STOP_TIMESTAMP=$(date --date=$STOP +%s)

echo $(($STOP_TIMESTAMP-$START_TIMESTAMP)) seconds
