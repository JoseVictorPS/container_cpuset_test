#include "gen_stack.h"
#include <time.h>

int main(void) {

    clock_t comeco = clock();

    STK * pilha_um = create_s();

    long count = 0;

    clock_t comeco_aloca = clock();

    while(count < 14) {
        push(pilha_um);
        count++;
    }

    clock_t fim_aloca = clock();
    double tempo_aloca = (double)(fim_aloca - comeco_aloca) / CLOCKS_PER_SEC;
    printf("\nO TEMPO GASTO NA ALOCACAO: %lf SEGUNDOS\n\n", tempo_aloca);

    clock_t comeco_int = clock();

    int a = 0;
    while(a < 999999999) {
        a++;
    }
    int b = 0;
    while(b < 999999999) {
        b++;
    }
    int c = 0;
    while(c < 999999999) {
        c++;
    }

    clock_t fim_int = clock();
    double tempo_int = (double)(fim_int - comeco_int) / CLOCKS_PER_SEC;
    printf("\nO TEMPO GASTO NO INCREMENTO: %lf SEGUNDOS\n\n", tempo_int);

    clock_t comeco_free = clock();

    free_s(pilha_um);

    clock_t fim_free = clock();
    double tempo_free = (double)(fim_free - comeco_free) / CLOCKS_PER_SEC;
    printf("\nO TEMPO GASTO LIBERANDO: %lf SEGUNDOS\n\n", tempo_free);

    clock_t fim = clock();
    double tempo_gasto = (double)(fim - comeco) / CLOCKS_PER_SEC;
    printf("\nO TEMPO TOTAL GASTO: %lf SEGUNDOS\n\n", tempo_gasto);

    return 0;
}
